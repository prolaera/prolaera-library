const authorDeliveryMethods: Array<{
  delivery_uid: number;
  type: string;
}> = [
  {
    delivery_uid: 0,
    type: 'Self-Study'
  },
  {
    delivery_uid: 1,
    type: 'Group-Live'
  },
  {
    delivery_uid: 2,
    type: 'Group-Internet / Webinar'
  },
  {
    delivery_uid: 8,
    type: 'QAS Self-Study'
  },
  {
    delivery_uid: 9,
    type: 'Nano-Learning'
  },
  {
    delivery_uid: 10,
    type: 'Blended Learning (Group-Live)'
  },
  {
    delivery_uid: 11,
    type: 'Blended Learning (Group-Internet)'
  }

];

export {authorDeliveryMethods};
