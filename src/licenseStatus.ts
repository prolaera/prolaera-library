const licenseStatus: Array<{
  [key: string]: number | string;
  id: number;
  name: string;
}> = [
  {
    id: 1,
    name: 'At Risk'
  },
  {
    id: 2,
    name: 'Behind Benchmark'
  },
  {
    id: 3,
    name: 'Exceeding Benchmark'
  }
];

export { licenseStatus };
