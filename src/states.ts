const states: {
  [key: string]: {
    [key: string]: string;
    name: string;
    id: string;
    short: string;
  };
} = {
  AL: {
    name: 'Alabama',
    id: 'AL',
    short: 'AL'
  },
  AK: {
    name: 'Alaska',
    id: 'AK',
    short: 'AK'
  },
  AZ: {
    name: 'Arizona',
    id: 'AZ',
    short: 'AZ'
  },
  AR: {
    name: 'Arkansas',
    id: 'AR',
    short: 'AR'
  },
  CA: {
    name: 'California',
    id: 'CA',
    short: 'CA'
  },
  CO: {
    name: 'Colorado',
    id: 'CO',
    short: 'CO'
  },
  CT: {
    name: 'Connecticut',
    id: 'CT',
    short: 'CT'
  },
  DE: {
    name: 'Delaware',
    id: 'DE',
    short: 'DE'
  },
  DC: {
    name: 'District of Columbia',
    id: 'DC',
    short: 'DC'
  },
  FL: {
    name: 'Florida',
    id: 'FL',
    short: 'FL'
  },
  GA: {
    name: 'Georgia',
    id: 'GA',
    short: 'GA'
  },
  HI: {
    name: 'Hawaii',
    id: 'HI',
    short: 'HI'
  },
  ID: {
    name: 'Idaho',
    id: 'ID',
    short: 'ID'
  },
  IL: {
    name: 'Illinois',
    id: 'IL',
    short: 'IL'
  },
  IN: {
    name: 'Indiana',
    id: 'IN',
    short: 'IN'
  },
  IA: {
    name: 'Iowa',
    id: 'IA',
    short: 'IA'
  },
  KS: {
    name: 'Kansas',
    id: 'KS',
    short: 'KS'
  },
  KY: {
    name: 'Kentucky',
    id: 'KY',
    short: 'KY'
  },
  LA: {
    name: 'Louisiana',
    id: 'LA',
    short: 'LA'
  },
  ME: {
    name: 'Maine',
    id: 'ME',
    short: 'ME'
  },
  MD: {
    name: 'Maryland',
    id: 'MD',
    short: 'MD'
  },
  MA: {
    name: 'Massachusetts',
    id: 'MA',
    short: 'MA'
  },
  MI: {
    name: 'Michigan',
    id: 'MI',
    short: 'MI'
  },
  MN: {
    name: 'Minnesota',
    id: 'MN',
    short: 'MN'
  },
  MS: {
    name: 'Mississippi',
    id: 'MS',
    short: 'MS'
  },
  MO: {
    name: 'Missouri',
    id: 'MO',
    short: 'MO'
  },
  MT: {
    name: 'Montana',
    id: 'MT',
    short: 'MT'
  },
  NE: {
    name: 'New England',
    id: 'NE',
    short: 'NE'
  },
  NV: {
    name: 'Nevada',
    id: 'NV',
    short: 'NV'
  },
  NH: {
    name: 'New Hampshire',
    id: 'NH',
    short: 'NH'
  },
  NJ: {
    name: 'New Jersey',
    id: 'NJ',
    short: 'NJ'
  },
  NM: {
    name: 'New Mexico',
    id: 'NM',
    short: 'NM'
  },
  NY: {
    name: 'New York',
    id: 'NY',
    short: 'NY'
  },
  NC: {
    name: 'North Carolina',
    id: 'NC',
    short: 'NC'
  },
  ND: {
    name: 'North Dakota',
    id: 'ND',
    short: 'ND'
  },
  OH: {
    name: 'Ohio',
    id: 'OH',
    short: 'OH'
  },
  OK: {
    name: 'Oklahoma',
    id: 'OK',
    short: 'OK'
  },
  OR: {
    name: 'Oregon',
    id: 'OR',
    short: 'OR'
  },
  PA: {
    name: 'Pennsylvania',
    id: 'PA',
    short: 'PA'
  },
  RI: {
    name: 'Rhode Island',
    id: 'RI',
    short: 'RI'
  },
  SC: {
    name: 'South Carolina',
    id: 'SC',
    short: 'SC'
  },
  SD: {
    name: 'South Dakota',
    id: 'SD',
    short: 'SD'
  },
  TN: {
    name: 'Tennessee',
    id: 'TN',
    short: 'TN'
  },
  TX: {
    name: 'Texas',
    id: 'TX',
    short: 'TX'
  },
  UT: {
    name: 'Utah',
    id: 'UT',
    short: 'UT'
  },
  VT: {
    name: 'Vermont',
    id: 'VT',
    short: 'VT'
  },
  VA: {
    name: 'Virginia',
    id: 'VA',
    short: 'VA'
  },
  WA: {
    name: 'Washington',
    id: 'WA',
    short: 'WA'
  },
  WV: {
    name: 'West Virginia',
    id: 'WV',
    short: 'WV'
  },
  WI: {
    name: 'Wisconsin',
    id: 'WI',
    short: 'WI'
  },
  WY: {
    name: 'Wyoming',
    id: 'WY',
    short: 'WY'
  }
};

export { states };
