const skins: Array<{
  [key: string]: string;
  name: string;
  value: string;
}> = [
  {
    name: 'Blue',
    value: 'skin-prolaera'
  },
  {
    name: 'Black',
    value: 'skin-black'
  },
  {
    name: 'Gray',
    value: 'skin-gray'
  },
  {
    name: 'Light Blue',
    value: 'skin-light-blue'
  },
  {
    name: 'Orange',
    value: 'skin-orange'
  },
  {
    name: 'White',
    value: 'skin-white'
  }
];

export { skins };
