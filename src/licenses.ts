const licenses: {
  [key: string]: string;
} = {
  yellowbook: 'Yellowbook'
};

export { licenses };
