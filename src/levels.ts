const levels: string[] = [
  'Advanced',
  'Basic',
  'Intermediate',
  'Overview',
  'Update'
];

export { levels };
