const sponsors: Array<{
  [key: string]: string | {} | undefined;
  description: string;
  key: string;
  name: string;
  fields: {
    [key: string]: boolean;
    checkbox: boolean;
    input: boolean;
  };
  placeholder: string;
  mask?: string;
}> = [
  {
    description:
      'For the sponsor ID look on your certificate for a NASBA number or National Registry number.',
    key: 'nationalRegistry',
    name: 'National Registry (NASBA)',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'National Registry ID #'
  },
  {
    description:
      'CPE courses approved by this state will have a sponsor ID listed.',
    key: 'newYork',
    name: 'New York',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'New York ID #'
  },
  {
    description:
      'CPE courses sponsored by QAS will have number listed somewhere below the course title.',
    key: 'qAS',
    name: 'NASBA QAS',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'QAS ID #'
  },
  {
    description:
      'CPE courses approved by this state will have a sponsor ID listed.',
    key: 'newJersey',
    name: 'New Jersey',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'New Jersey ID #'
  },
  {
    description:
      'Typically this is only for EAs. Courses that have IRS approval will list the approval number under the course title. It may say approval number as opposed to sponsor ID.',
    key: 'irs',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'Approval',
    name: 'IRS Approval Code'
  },
  {
    description:
      'Per the board, the only CE course that requires an “Approval Number” is the Board Approved Regulatory Review course. This approval number can be found directly on the certificate of completion, format: RXX-###-####.',
    key: 'californiaRR',
    name: 'California',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'California Regulatory Review #',
    mask: 'RRS-9?9?9?9?9?9?9?9?9?9'
  },
  {
    description:
      'CPE courses approved by this state will have a sponsor ID listed.',
    key: 'pennsylvania',
    name: 'Pennsylvania',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'Pennsylvania ID #'
  },
  {
    description:
      'CPE courses approved by this state will have a sponsor ID listed.',
    key: 'illinois',
    name: 'Illinois',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'Illinois ID #'
  },
  {
    description:
      'CPE courses approved by this state will have a sponsor ID listed.',
    key: 'texas',
    name: 'Texas',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'Texas ID #'
  },
  {
    description:
      'CPE courses approved by this society will have a sponsor ID listed.',
    key: 'society',
    name: 'Society',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'Society ID #'
  },
  {
    description: "Have a sponsor ID that we haven't mentioned? Put it here.",
    key: 'allOthers',
    name: 'All Others',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: "All Others ID #'s"
  },
  {
    description: 'CPE courses sponsored by your firm? Put it here',
    key: 'inFirm',
    name: 'In-Firm',
    fields: {
      checkbox: true,
      input: true
    },
    placeholder: 'N/A'
  },
  {
    description: 'If no sponsor information is provided, check this box.',
    key: 'noSponsor',
    name: 'None Provided',
    fields: {
      checkbox: true,
      input: false
    },
    placeholder: 'N/A'
  }
];

export { sponsors };
