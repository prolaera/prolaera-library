const territories: {
  [key: string]: {
    [key: string]: string;
    name: string;
    id: string;
    short: string;
  };
} = {
  pr: {
    name: 'Puerto Rico',
    id: 'PR',
    short: 'PR'
  }
};

export { territories };
