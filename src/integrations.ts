const integrations = [
  {
    id: 'acpen',
    name: 'ACPEN',
    fields: {
      clientId: {
        type: 'text',
        label: 'ClientId',
        required: true
      },
      secret: {
        type: 'password',
        label: 'Secret',
        required: true
      }
    },
    usage: ['events']
  },
  {
    id: 'go_to_webinar',
    name: 'Go To Webinar',
    fields: {
      client_id: {
        type: 'text',
        label: 'Consumer Key',
        required: true
      },
      client_secret: {
        type: 'password',
        label: 'Consumer Secret',
        required: true
      }
    },
    process: 'gtwOauth',
    usage: ['events']
  },
  {
    id: 'webex',
    name: 'WebEx',
    fields: {
      sitename: {
        type: 'text',
        label: 'Site name',
        required: true
      },
      hostid: {
        type: 'text',
        label: 'Host Username / Email',
        required: true
      },
      hostpassword: {
        type: 'password',
        label: 'Host password',
        required: true
      }
    },
    usage: ['events']
  },
  {
    id: 'scorm',
    name: 'Scorm',
    fields: {
      appId: {
        type: 'text',
        label: 'App ID',
        required: true
      },
      appTitle: {
        type: 'text',
        label: 'App title',
        required: true
      },
      appSecret: {
        type: 'password',
        label: 'App secret key',
        required: true
      }
    },
    usage: ['events', 'courses']
  },
  {
    id: 'cnf',
    name: 'Conference.io',
    fields: {
      sitename: {
        type: 'text',
        label: 'Site name',
        required: true
      },
      apiKey: { type: 'password', label: 'Api Key', required: true }
    },
    usage: ['events']
  },
  {
    id: 'zoom',
    name: 'Zoom',
    fields: {
      userId: {
        type: 'text',
        label: 'User Id',
        required: true
      },
      accountId: {
        type: 'text',
        label: 'Account Id',
        required: true
      },
      clientId: {
        type: 'text',
        label: 'Client Id',
        required: true
      },
      clientSecret: {
        type: 'password',
        label: 'Client Secret',
        required: true
      }
    },
    usage: ['events']
  },
  {
    id: 'boosterLearn',
    name: 'BoosterLearn',
    fields: {
      key: {
        type: 'text',
        label: 'Access Key',
        required: true
      },
      secret: {
        type: 'password',
        label: 'Secret',
        required: true
      }
    },
    usage: ['events']
  }
];

export { integrations };
