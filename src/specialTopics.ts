const specialTopics: Array<{
  [key: string]: any;

  key: string;
  name: string;
  secondary_key?: string;
  compliance_key?: string;
  short: string;
}> = [
  {
    key: 'businessValuation',
    name: 'Business Valuation',
    short: 'BV'
  },
  {
    key: 'coachingMentoring',
    name: 'Coaching / Mentoring',
    short: 'CM'
  },
  {
    key: 'constructionSpecific',
    name: 'Construction Specific',
    short: 'CS'
  },
  {
    key: 'employeeBenefitPlan',
    secondary_key: 'employee_benefit',
    name: 'Employee Benefit Plans',
    short: 'EBP'
  },
  {
    key: 'federalTaxes',
    name: 'Federal Taxes',
    short: 'FT'
  },
  {
    key: 'financialForensics',
    name: 'Financial Forensics',
    short: 'FF'
  },
  {
    key: 'financialPlanning',
    name: 'Financial Planning',
    short: 'FP'
  },
  {
    key: 'fraud',
    name: 'Fraud',
    short: 'F'
  },
  {
    key: 'fsp',
    name: 'Financial Statement Presentation',
    short: 'FSP'
  },
  {
    key: 'hud',
    name: 'Housing and Urban Development',
    short: 'HUD'
  },
  {
    key: 'shpt',
    secondary_key: 'sexual_harassment',
    name: 'Sexual Harassment Prevention Training',
    short: 'SHPT'
  },
  {
    key: 'yellowbook',
    compliance_key: 'proficiency',
    name: 'Yellowbook Proficiency',
    short: 'YB'
  }
];

export {specialTopics};
