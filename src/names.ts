const names: {
  [key: string]: string;
} = {
  a_a: 'A&A',
  agent_training: 'Acceptance Agent Training',
  annual_a_a: 'Annual A&A',
  count_a_a: 'A&A',
  a_a_g: 'Government A&A',
  a_a_ethics: 'A&A Ethics',
  a_a_t: 'A&A & Tax',
  accounting: 'Accounting',
  'accounting auditing': 'A&A',
  'accounting auditing tax': 'A&A & Tax',
  advisory_services: 'Advisory Services',
  aga: 'AGA',
  aicpa: 'AICPA',
  aira: 'AIRA',
  allothers: 'All Others',
  apa: 'APA',
  asppa: 'ASPPA',
  auditing: 'Auditing',
  bai: 'BAI',
  behavioural: 'Behavioural',
  business: 'Business',
  business_strategy: 'Business Managent & Strategy',
  business_acumen: 'Business Acumen / Specialized Knowledge',
  businessvaluation: 'Business Valuation',
  cap: 'CAP',
  count_a_a_g: 'Government A&A',
  ccsp: 'CCSP',
  ccifp: 'CCIFP',
  cfa: 'CFA',
  cfe: 'CFE',
  cfp: 'CFP',
  cissp: 'CISSP',
  coachingmentoring: 'Coaching / Mentoring',
  comptia: 'CompTIA',
  computer: 'Computer',
  constructionspecific: 'Construction Specific',
  cpa: 'CPA',
  'cpa attestation': 'CPA Attestation',
  'cpa government auditing': 'CPA Government Auditing',
  csa: 'CSA',
  csslp: 'CSSLP',
  ctec: 'CTEC',
  ea: 'EA',
  eccouncil: 'EC-Council',
  employee_benefit: 'Employee Benefits',
  ethics_accounting_practice: 'Ethics - Practice of Accounting',
  ethics_general: 'Ethics General',
  count_ethics_general: 'Ethics General',
  ethics_state: 'Ethics State',
  fa: 'FA',
  federal_taxes: 'Federal Taxes',
  federalTaxes: 'Federal Taxes',
  finra: 'FINRA',
  firm_element: 'Firm Element',
  fraud: 'Fraud',
  forensic_training: 'Forensic Training',
  gaap_ssars: 'GAAP / SSARS',
  governmental: 'Governmental',
  'government accounting auditing': 'Government A&A',
  group_a: 'Group A',
  group_a_b: 'Group A or B',
  group_b: 'Group B',
  hfma: 'HFMA',
  hims: 'HiMS',
  hitrust: 'HITRUST',
  hours: 'General Hours',
  hrci: 'HRCI',
  iapp: 'IAPP',
  idfa: 'IDFA',
  iia: 'IIA',
  ipt: 'IPT',
  ipt_ethics: 'IPT Approved Ethics',
  ima: 'IMA',
  income_franchise_tax: 'Income/Franchise Taxation',
  instruction: 'Instruction',
  irs: 'IRS',
  isaca: 'ISACA',
  isc2: 'ISC2',
  iso: 'ISO',
  isp: 'ISP',
  iwi: 'IWI',
  lunch_learn: 'Lunch & Learn',
  management: 'Management',
  management_operations: 'Management & Operations',
  nacpb: 'NACPB',
  nacva: 'NACVA',
  namsss: 'NAMSSS',
  namss_approved: 'NAMSSS Approved',
  nationalregistry: 'National Registry',
  newjersey: 'New Jersey',
  newyork: 'New York',
  nipr: 'NIPR',
  nls: 'None Legal Subject',
  nontech: 'Non-Tech',
  pcaob: 'PCAOB',
  psr: 'PSR',
  pci: 'PCI',
  pecb: 'PECB',
  'personal development': 'Personal Dev.',
  personal_development: 'Personal Development',
  pmi: 'PMI',
  property_tax: 'Property Taxation',
  pro_ethics: 'Professional Ethics',
  proficiency: 'Professional Proficiency',
  publication: 'Publication',
  qas: 'QAS',
  quickbooks: 'QuickBooks',
  regulatory_element: 'Regulatory Element',
  regulatoryreview: 'Regulatory Review',
  sales_tax: 'Sales Taxation',
  self_study: 'Self Study',
  nano_learning: 'Nano-Learning',
  blended_learning: 'Blended Learning',
  specialized_knowledge: 'Specialized Knowledge',
  sscp: 'SSCP',
  'ssf in-firm requirement': 'SSF In-Firm Requirement',
  'strategic business management': 'Strategic & Business Management',
  tax: 'Tax',
  tax_ethics: 'Tax Ethics',
  taxation: 'Taxation',
  tech: 'Technical',
  tech_business: 'Technical Business',
  yellowbook: 'Governmental A&A',
  fsp: 'Financial Statement Presentation',
  total: 'Total',
  'diversity inclusion and elimination of bias':
    'Diversity, Inclusion and Elimination of Bias',
  diversity: 'Diversity, Inclusion and Elimination of Bias',
  skills: 'Skills',
  'ethics and professionalism': 'Ethics and Professionalism',
  ethics_professionalism: 'Ethics and Professionalism',
  ethics_regulatory: 'Ethics Regulatory',
  ethics_behavioral: 'Ethics Behavioral',
  law_practice_management: 'Law Practice Management',
  professional_practice: 'Areas of Professional Practice',
  areas_professional_practice:
    'Law Practice Management and/or Areas of Professional Practice',
  'law practice management and or areas of professional practice':
    'Law Practice Management and/or Areas of Professional Practice',
  sexual_harassment: 'Sexual Harassment Prevention Training',
  ce: 'CE',
  continuing_education: 'Continuing Education',
  pa: 'PA',
  professional_activity: 'Professional Activity',
  uspap: 'USPAP',
  uniform_standards_professional_appraisal_practice:
    'Uniform Standards of Professional Appraisal Practice',
  mental_health_substance_abuse: 'Mental Health/Substance Abuse',
  diversity_inclusion: 'Diversity/Inclusion',
  professional_responsibility: 'Professional Responsibility',
  competence_issues: 'Competence Issues',
  legal_ethics: 'Legal Ethics',
  recognition_elimination_bias: 'Recognition and Elimination of Bias',
  hfma_approved: 'HFMA Approved',
  giving_back: 'Giving Back',
  leadership: 'Leadership',
  ssa: 'SSA',
  technology: 'Technology',
  internal_processes: 'Internal Processes',
  informationTechnology: 'Information Technology',
  constructionSpecific: 'Construction Specific',
  category_2: 'Category 2',
  category_1: 'Category 1',
  current_a_a: 'Current Annual A&A',
  federal_tax_law: 'Federal Tax Law',
  ca_tax_law: 'California Tax Law',
  federal_tax_law_updates: 'Federal Tax Law Updates',
  life_skills: 'Life Skills',
  coaching: 'Coaching',
  communications: 'Communications',
  coachingMentoring: 'Coaching Mentoring',
  regulatoryReview: 'Regulatory Review',
  board_approved_ethics: 'Board Approved Ethics',
  financialPlanning: 'Financial Planning',
  california_credits: 'California Credits',
  businessValuation: 'Business Valuation',
  general_ethics: 'General Ethics',
  investigations: 'Investigations',
  laws_rules_review: 'Laws Rules Review',
  divorce: 'Divorce',
  legislative_updates: 'Legislative Updates',
  financial_planning_ethics: 'Financial Planning Ethics',
  financialForensics: 'Financial Forensics',
  firma_specific: 'FIRMA Specific',
  federal_tax_law_topics: 'Federal Tax Law Topics',
  pre_cpa: 'Pre CPA',
  business_acumen_only: 'Business Acumen',
  power_skills: 'Power Skills',
  ways_of_working: 'Ways of Working'
};

export { names };
