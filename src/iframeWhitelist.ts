const iframeWhitelist: string[] = [
  'anchor.fm',
  'fast.wistia.com',
  'files.prolaera.com',
  'forms.office.com',
  'pbs.twimg.com',
  'player.vimeo.com',
  'players.brightcove.net',
  'video.tv.adobe.com',
  'web.microsoftstream.com',
  'www.youtube.com'
];

export { iframeWhitelist };
