const donutGaugeColors: {
  [key: string]: string;
} = {
  default: '#eeeeee',
  remaining: '#eeeeee',
  basic: '#483d8b',
  white: '#FFFFFF',
  info: '#23c6c8',
  danger: '#ED5565',
  warning: '#f8ac59',
  success: '#1c84c6',
  primary: '#72c02c'
};

export { donutGaugeColors };
