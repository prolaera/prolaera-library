const deliveryMethods: Array<{
  key: number; name: string; active: boolean, nasba_label: string, delivery_types?: {
    activity?: boolean;
    event?: boolean;
    self_study?: boolean;
    webinar?: boolean;
  }
}> = [
  {
    key: 0,
    name: 'Self-Study',
    nasba_label: 'Self-Study',
    delivery_types: {
      self_study: true
    },
    active: true
  },
  {
    key: 1,
    name: 'Group-Live',
    nasba_label: 'Group-Live',
    delivery_types: {
      event: true
    },
    active: true
  },
  {
    key: 2,
    name: 'Group-Internet Based',
    nasba_label: 'Group-Internet Based',
    delivery_types: { webinar: true, event: true },
    active: true
  },
  {
    key: 3,
    name: 'Publication',
    nasba_label: 'Publication',
    active: true
  },
  {
    key: 4,
    name: 'Instruction',
    nasba_label: 'Instruction',
    active: true
  },
  {
    key: 5,
    name: 'University',
    nasba_label: 'University',
    delivery_types: { event: true },
    active: true
  },
  {
    key: 6,
    name: 'Self-Study (Non-Interactive)',
    nasba_label: 'Self-Study (Non-Interactive)',
    delivery_types: { self_study: true },
    active: true
  },
  {
    key: 7,
    name: 'Carry Over',
    nasba_label: 'Carry Over',
    active: false
  },
  {
    key: 8,
    name: 'QAS Self-Study',
    nasba_label: 'QAS Self-Study',
    delivery_types: { self_study: true },
    active: true
  },
  {
    key: 9,
    name: 'Nano-Learning',
    nasba_label: 'Nano-Learning',
    delivery_types: { activity: true },
    active: true
  },
  {
    key: 10,
    name: 'Blended Learning (Group-Live)',
    nasba_label: 'Blended Learning (Group-Live)',
    delivery_types: { event: true },
    active: true
  },
  {
    key: 11,
    name: 'Blended Learning (Group-Internet)',
    nasba_label: 'Blended Learning (Group-Internet)',
    delivery_types: { event: true, webinar: true },
    active: true
  },
  {
    key: 12,
    name: 'Informal Learning Activity',
    nasba_label: 'Informal Learning Activity',
    delivery_types: { activity: true },
    active: true
  }
];

export { deliveryMethods };
