const defaultSponsorStatements: {
  [key: string]: string;
} = {
  sponsorStatement:
    'In accordance with the standards of the National Registry of CPE Sponsors and as set forth in Circular 230 Section 10.6, credits have been granted based on a 50-minute hour. State boards of accountancy, state bars, and other state regulatory bodies have final authority on the acceptance of courses for continuing education credit. Please check with your state board regarding applicability of this course towards your continuing education requirement.'
};

export { defaultSponsorStatements };
