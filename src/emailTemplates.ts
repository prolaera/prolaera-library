import { EmailTemplates } from '@prolaera/types';

const emailTemplates: Array<{
  [key: string]: string | {};
  id: string;
  name: string;
  form: {
    [key: string]: boolean | {};
    fromName: boolean;
    title: string;
    description: string;
    messages: {
      [key: string]: boolean;
      body: boolean;
      conclusion: boolean;
      intro: boolean;
    };
    replyTo: boolean;
    subject: boolean;
  };
  usage: string;
}> = [
  {
    id: EmailTemplates.eventCertificate,
    name: 'Event Certificate Has Been Issued',
    form: {
      title: 'Event Certificate Has Been Issued',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been issued a group-live course certificate.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true,
      eventInfoOptions: {
        includeAudience: true,
        includeCredits: true,
        includeLevel: true,
        includeObjectives: true,
        includePrep: true,
        includePrerequisites: true,
        includePrice: false,
        includeSummary: true
      },
      includeEventEvaluationLink: true
    },
    usage: 'events'
  },
  {
    id: EmailTemplates.eventEvaluation,
    name: 'Event Evaluation Request',
    form: {
      title: 'Event Evaluation Request',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been issued a group-live course certificate.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true,
      eventInfoOptions: {
        includeAudience: true,
        includeCredits: true,
        includeLevel: true,
        includeObjectives: true,
        includePrep: true,
        includePrice: false,
        includePrerequisites: true,
        includeSummary: true
      }
    },
    usage: 'events'
  },
  {
    id: EmailTemplates.eventInvitation,
    name: 'User Has Been Invited to Attend an Event',
    form: {
      title: 'User Has Been Invited to Attend an Event',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been invited invited to attend an event.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true,
      eventInfoOptions: {
        includeAudience: true,
        includeCredits: true,
        includeLevel: true,
        includeObjectives: true,
        includePrep: true,
        includePrerequisites: true,
        includePrice: true,
        includeSummary: true
      }
    },
    usage: 'events'
  },
  {
    id: EmailTemplates.eventPostWork,
    name: 'User Has Post-Work for an Event',
    form: {
      title: 'User Has Post-Work for an Event',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has post-work for an event.',
      fromName: false,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: false,
      subject: false
    },
    usage: 'events'
  },
  {
    id: EmailTemplates.eventRegistration,
    name: 'User Has Been Registered to Attend an Event',
    form: {
      title: 'User Has Been Registered to Attend an Event',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been directly registered to an event.',
      fromName: false,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: false,
      subject: true,
      eventInfoOptions: {
        includeAudience: true,
        includeCredits: true,
        includeLevel: true,
        includeObjectives: true,
        includePrep: true,
        includePrerequisites: true,
        includePrice: true,
        includeSummary: true
      }
    },
    usage: 'events'
  },
  {
    id: EmailTemplates.eventRegistrationReminder,
    name: 'Upcoming Event Reminder',
    form: {
      title: 'Upcoming Event Reminder',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is manually triggered when a user has been registered to attend an event.',
      fromName: false,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: false,
      subject: true,
      eventInfoOptions: {
        includeAudience: true,
        includeCredits: true,
        includeLevel: true,
        includeObjectives: true,
        includePrep: true,
        includePrerequisites: true,
        includePrice: true,
        includeSummary: true
      }
    },
    usage: 'events'
  },
  {
    id: EmailTemplates.eventRegistrationUpdate,
    name: 'User’s Registration Has Been Updated',
    form: {
      title: 'User’s Registration Has Been Updated',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user’s event registration has been canceled or rescheduled.',
      fromName: false,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: false,
      subject: true,
      eventInfoOptions: {
        includeAudience: true,
        includeCredits: true,
        includeLevel: true,
        includeObjectives: true,
        includePrep: true,
        includePrerequisites: true,
        includePrice: true,
        includeSummary: true
      }
    },
    usage: 'events'
  },
  {
    id: EmailTemplates.firmMigrate,
    name:
      'User Has Been Invited to Merge His/Her Personal Account With a Firm Account',
    form: {
      title:
        'User Has Been Invited to Merge His/Her Personal Account With a Firm Account',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been invited to merge an individual account with a Firm account.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'users'
  },
  {
    id: EmailTemplates.courseFeedback,
    name: 'Self-Study Course Evaluation Request',
    form: {
      title: 'Self-Study Course Evaluation Request',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered to request feedback from a user who has completed a self-study course.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'courses'
  },
  {
    id: EmailTemplates.courseReview,
    name: 'Course Has Been Submitted for Review',
    form: {
      title: 'Course Has Been Submitted for Review',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a course has been created and submitted by either an author, an admin or both.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'courses'
  },
  {
    id: EmailTemplates.firmInvite,
    name: 'User Has Been Invited to Join the Firm',
    form: {
      title: 'User Has Been Invited to Join the Firm',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when an admin has invited a user to join the firm for the first time.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'users'
  },
  {
    id: EmailTemplates.teamLeaderAssigned,
    name: 'User Has Been added to a Team as a Team Leader',
    form: {
      title: 'User Has Been added to a Team as a Team Leader',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been assigned a team and given permission to lead their team.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'teams'
  },
  {
    id: EmailTemplates.trackAssigned,
    name: 'User Has Been Assigned a Learning Track',
    form: {
      title: 'User Has Been Assigned a Learning Track',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been added to a learning track.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'tracks'
  },
  {
    id: EmailTemplates.complianceReport,
    name: 'User’s Compliance Report Status',
    form: {
      title: 'User’s Compliance Report Status',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is sent out on a monthly or quarterly basis to individual users. This email contains a high-level overview of the user’s compliance report status for each jurisdiction they track.',
      detailReport: true,
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'users'
  },
  {
    id: EmailTemplates.accountActivated,
    name: 'User’s Account Has Been Reactivated',
    form: {
      title: 'User’s Account Has Been Reactivated',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user’s account has been reactivated.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'users'
  },
  {
    id: EmailTemplates.accountArchived,
    name: 'User’s Account Has Been Archived',
    form: {
      title: 'User’s Account Has Been Archived',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user’s account has been archived.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'users'
  },
  {
    id: EmailTemplates.accountMigrated,
    name: 'User’s Account Has Been Archived and Migrated',
    form: {
      title: 'User’s Account Has Been Archived and Migrated',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when an admin has archived a user’s account and migrated their data to a personal account.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'users'
  },
  {
    id: EmailTemplates.activityAssigned,
    name: 'User Has Been Assigned an Activity',
    form: {
      title: 'User Has Been Assigned an Activity',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when an admin has assigned an activity to a user.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'activities'
  },
  {
    id: EmailTemplates.courseActivated,
    name: 'Course Has Been Activated',
    form: {
      title: 'Course Has Been Activated',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when an admin approves and activates a course submitted by an author.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'courses'
  },
  {
    id: EmailTemplates.courseApproved,
    name: 'Course Has Been Approved',
    form: {
      title: 'Course Has Been Approved',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when an admin approves and activates a course submitted by an author.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'courses'
  },
  {
    id: EmailTemplates.courseAssigned,
    name: 'Course Has Been Assigned',
    form: {
      title: 'Course Has Been Assigned',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been assigned a self-study course.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'courses'
  },
  {
    id: EmailTemplates.courseCertificate,
    name: 'Self-Study Course Certificate Has Been Issued',
    form: {
      title: 'Self-Study Course Certificate Has Been Issued',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been issued a self-study course certificate.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'courses'
  },
  {
    id: EmailTemplates.courseRegistration,
    name: 'User Has Been Registered to a Self-Study Course',
    form: {
      title: 'User Has Been Registered to a Self-Study Course',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has been directly registered to take a self-study course.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true
    },
    usage: 'courses'
  },
  {
    id: EmailTemplates.eventWaitlist,
    name: 'User Has Been Waitlisted',
    form: {
      title: 'User Has Been Waitlisted',
      description:
        'Customizing this email template will replace the messaging used in Prolaera’s default email which is triggered when a user has registered / been registered to an event that is at capacity limit and was waitlisted instead.',
      fromName: true,
      messages: {
        body: false,
        conclusion: true,
        intro: true
      },
      replyTo: true,
      subject: true,
      eventInfoOptions: {
        includeAudience: true,
        includeCredits: true,
        includeLevel: true,
        includeObjectives: true,
        includePrep: true,
        includePrerequisites: true,
        includeSummary: true
      }
    },
    usage: 'events'
  }
];

export { emailTemplates };
