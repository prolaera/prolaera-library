# Prolaera Library

This package consists of things that really ought to be stored as configuration in the database or migrated to enums. This package is a stop-gap until that work takes place.

This package is a dependency for some of our other packages, and helps to keep them in sync.

## Development

### Prerequisites

- TypeScript

### Installation

```
npm install
```

This mainly just pulls in `@prolaera/types` as a dependency, and runs a script that massages the type repo into a format that can be used by this package such that it can be compiled.

### Coding style

Just basic TypeScript. We haven't gone crazy with linting our anything in this repo, as it basically just produces a barrel file full of simple objects.

### Git strategy

- Keep commits small and targeted
- Preface commit messages with the ID of the story
- Get at least one approval before merging
- Merge strategy is fastforward, so no merge commits

For example:

```
git checkout -b feature/myfeature
git add .
git commit -m "5551212 Updating blah blah"
git fetch
git rebase origin/master
git push origin feature/myfeature
```

### Local deployment

For local development, build the library locally, and use NPM link to link your local library into the consuming project. For instance, if you wanted to develop and test your changes in the API:

In your library package directory, run:

```
npm run build
```

And from your local API directory, run:

```
npm link ../prolaera_library
```

where `../prolaera_library` is the relative path to your local library repo.

## Publishing

This repo is deployed as a private NPM package, and consumed by other Prolaera repos. Deployment specifics are documented in `bitbucket-pipelines.yml` -- at a high level, it runs `npm build`, which builds a barrel file and then runs TypeScript to build a `dist` folder. The pipeline then increments the version and publishes it to our private NPM namespace.

In the consuming package run:

```
npm install @prolaera/library@latest
```

to increment Prolaera Library to the latest version.

*Note:* other dependent packages will need to be at the same version, so the Client and the API will want to be on the same version of this library.